# My dmenu build
Patches:
- Center - Centers dmenu
- Border - Adds border
- Fuzzy(highlight/match) - Fuzzy searches and highlights fuzzy searches
- Numbers - Adds text which displays the number of matched and total items in the top right corner of dmenu.
- Case Insensitive
- Tokyo Night - Color scheme
